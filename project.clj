(defproject trellodonelist "0.1.0-SNAPSHOT"
  :description "FIXME: write this!"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}

  :min-lein-version "2.6.1"
  
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [ring "1.4.0"]
                 [compojure "1.5.0"]
                 [ring/ring-defaults "0.2.0"]
                 [bk/ring-gzip "0.1.1"]
                 [org.clojure/clojurescript "1.7.228"]
                 [org.clojure/core.async "0.2.374"
                  :exclusions [org.clojure/tools.reader]]
                 [sablono "0.3.6"]
                 [com.andrewmcveigh/cljs-time "0.4.0"]
                 [org.omcljs/om "0.9.0"]]
  
  :plugins [[lein-figwheel "0.5.2"]
            [lein-cljsbuild "1.1.3" :exclusions [[org.clojure/clojure]]]]

  :source-paths ["src/clj" "src/cljs"]

  :uberjar-name "trellodonelist.jar"

  :main trellodonelist.server

  :clean-targets ^{:protect false} ["resources/public/js/compiled" "target"]

  :cljsbuild {:builds
    {:app
               {:source-paths ["src/cljs"]

                :figwheel true
                ;; Alternatively, you can configure a function to run every time figwheel reloads.
                ;; :figwheel {:on-jsload "chestnuttest.core/on-figwheel-reload"}

                :compiler {:main trellodonelist.core
                           :asset-path "js/compiled/out"
                           :output-to "resources/public/js/compiled/trellodonelist.js"
                           :output-dir "resources/public/js/compiled/out"
                           :source-map-timestamp true
                           :externs ["resources/public/js/externs.js"]}}}}
              ; [{:id "dev"
              ;   :source-paths ["src/cljs"]

              ;   ;; If no code is to be run, set :figwheel true for continued automagical reloading
              ;   :figwheel {:on-jsload "trellodonelist.core/on-js-reload"}

              ;   :compiler {:main trellodonelist.core
              ;              :asset-path "js/compiled/out"
              ;              :output-to "resources/public/js/compiled/trellodonelist.js"
              ;              :output-dir "resources/public/js/compiled/out"
              ;              :source-map-timestamp true}}
              ;  ;; This next build is an compressed minified build for
              ;  ;; production. You can build this with:
              ;  ;; lein cljsbuild once min
              ;  {:id "min"
              ;   :source-paths ["src/cljs"]
              ;   :compiler {:output-to "resources/public/js/compiled/trellodonelist.js"
              ;              :main trellodonelist.core
              ;              :optimizations :advanced
              ;              :pretty-print false}}]}

  :figwheel {;; :http-server-root "public" ;; default and assumes "resources"
             ;; :server-port 3449 ;; default
             ;; :server-ip "127.0.0.1"

             :css-dirs ["resources/public/css"] ;; watch and update CSS

             ;; Start an nREPL server into the running figwheel process
             ;; :nrepl-port 7888

             ;; Server Ring Handler (optional)
             ;; if you want to embed a ring handler into the figwheel http-kit
             ;; server, this is for simple ring servers, if this
             ;; doesn't work for you just run your own server :)
             ;; :ring-handler hello_world.server/handler

             ;; To be able to open files in your editor from the heads up display
             ;; you will need to put a script on your path.
             ;; that script will have to take a file path and a line number
             ;; ie. in  ~/bin/myfile-opener
             ;; #! /bin/sh
             ;; emacsclient -n +$2 $1
             ;;
             ;; :open-file-command "myfile-opener"

             ;; if you want to disable the REPL
             ;; :repl false

             ;; to configure a different figwheel logfile path
             ;; :server-logfile "tmp/logs/figwheel-logfile.log"
             }

  :profiles {:dev
             {:dependencies [[figwheel "0.5.2"]
                             [org.clojure/tools.nrepl "0.2.12"]]

              :plugins [[lein-figwheel "0.5.2"]
                        [lein-doo "0.1.6"]]

              :cljsbuild {:builds
                          {:test
                           {:source-paths ["src/cljs"]
                            :compiler
                            {:output-to "resources/public/js/compiled/testable.js"
                             :main chestnuttest.test-runner
                             :optimizations :none}}}}}

             :uberjar
             {:source-paths ^:replace ["src/clj"]
              :hooks [leiningen.cljsbuild]
              :omit-source true
              :aot :all
              :cljsbuild {:builds
                          {:app
                           {:source-paths ^:replace ["src/cljs"]
                            :compiler
                            {:optimizations :advanced
                             :pretty-print false}}}}}})
