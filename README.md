# Trello Done List

This is an application aimed solely at extracting actions performed in a [Trello](http://trello.com) board on a date selected by a user.

This goal is achieved using a combination of [ClojureScript](https://github.com/clojure/clojurescript), [Om](https://github.com/omcljs/om), [cljs-time](andrewmcveigh.github.io/cljs-time/uberdoc.html) and [Trello API](https://developers.trello.com/).

Even though this micro-project presents some practical value to me, it is 99% focused on trying and learning Om and ClojureScript.

# Logic

I use Trello (in particular) as a GTD action lists tool. Whenever I consider an action done, I archive (or, in the API terms, close) the corresponding card. Trello web version has limited capabilities in terms of viewing a structured list of cards archived on a certain date. On the other side, being able to check what I was able to accomplish, say, yesterday is sometimes crucial for my workflows and self-esteem. The present application closes this gaping hole for me rendering all cards archived on my daily board on the date that I specify. In addition to that, it pulls down the cards that were only commented, but not closed - I consider these relevant to the retrospective analysis as well.

# Limitations

- Security was the last thing I considered. Or, honestly, I didn't give it a single thought
- UX and UI are so modern that it may be dangerous to touch the application for anyone who has a strong sense of beuty
- Timezones are tricky. As such they are handled in a very limited manner
- The application is currently so general that it has some UIDs specific to my Trello hardcoded in it. If you happen to try it - be sure to locate every UID in the sources and change it to something suitable for you
- Finally, the thing is mostly about hack-n-slash, so code quality exceeds all expectations

## Setup

To get an interactive development environment run:

    lein figwheel

and open your browser at [localhost:3449](http://localhost:3449/).
This will auto compile and send all changes to the browser without the
need to reload. After the compilation process is complete, you will
get a Browser Connected REPL. An easy way to try it is:

## License

Copyright © 2016 Alexander Turok

Distributed under the Eclipse Public License either version 1.0 or (at your option) any later version.
