(ns trellodonelist.core
  (:require [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [cljs.core.async :as async]
            [cljs-time.core :as time]
            [cljs-time.format :as timeformat]
            [cljs-time.coerce :as timecoerce]
            )
  (:require-macros [cljs.core.async.macros :refer (go)]))

(enable-console-print!)

(println "Edits to this text should show up in your developer console.")

;; define your app data so that it doesn't get over-written on reload

(defonce comments-channel (async/chan))

(def date-formatter (timeformat/formatters :year-month-day))
(def full-date-formatter (timeformat/formatters :date-time))
(def render-date-formatter (timeformat/formatter "yyyy-MM-dd HH:mm"))

(defn today
  ([] (today 0))
  ([plus-days]
    (timeformat/unparse date-formatter (time/date-time (time/plus (time/today-at-midnight) (time/days plus-days))))))

(defonce app-state (atom {}))

(defn authorize-trello []
  (.authorize js/Trello #js {
    :type "popup"
    :interactive true
    :name "My Done List"
    :scope #js {"read" true "write" true}
    :expiration "never"
    :success (fn [] (print "success"))
    :failure (fn [] (print "failure"))
    }))

(defn parse-card-action [{{:keys [:card :text :old]} :data orig-action :type date :date}]
  (let [action-time (timeformat/parse full-date-formatter date)]
    (into {:date (time/to-default-time-zone action-time) :datetime-raw action-time}
      (cond
        (= orig-action "commentCard") {:name "commented" :data text}
        (and (= orig-action "updateCard") (:closed card) (not (:closed old))) {:name "closed"}
        (= orig-action "updateCard") {:name "updated"}
        true {:name "unknown"}))))

(defn card-url [{{{link-id :shortLink} :card} :data}]
  (str "https://trello.com/c/" link-id))

(defn parse-card [{{{card-name :name card-link :shortLink} :card {list-name :name} :list} :data action :type :as action-data}]
  {:action (parse-card-action action-data)
   :card-name card-name
   :card-id card-link
   :list-name list-name
   :url (card-url action-data)})

(defn internalize-actions [[card-id action-data-instances]]
  (-> (first action-data-instances)
    (dissoc :action)
    (dissoc :date)
    (assoc :actions (mapv :action action-data-instances))))

(defn display
  ([show attributes]
    (if show
      (clj->js attributes)
      (clj->js (assoc-in attributes [:style :display] "none"))))
  ([show]
    (display show nil)))

(defmulti render-action :name)

(defmethod render-action :default [{:keys [name date]}]
  (dom/li nil (str name " on " (timeformat/unparse render-date-formatter date))))

(defmethod render-action "commented" [{:keys [name date data]}]
  (dom/li #js {:className "comment-action" :onClick (fn [e] (async/put! comments-channel data))}
    (str name " on " (timeformat/unparse render-date-formatter date))))


(defn render-card [owner {:keys [card-name list-name url actions]} show-actions]
  (dom/div nil
    (dom/a #js {:href url} card-name)
    (dom/a #js {
          :className "button"
          :href "#"
          :onClick (fn [e] (om/update-state! owner :show-actions not))}
          (if show-actions "v" ">"))
    (apply dom/ul (display show-actions)
      (mapv
        render-action
        actions))))

(defn sort-cards [cards]
  (vec
    (sort-by
      (fn [{:keys [actions]}]
        (- (apply max (map (comp timecoerce/to-long :datetime-raw) actions))))
      cards)))

(defn create-lists [cards-data]
  (->> cards-data
    (map parse-card)
    (group-by :card-id)
    (mapv internalize-actions)
    (group-by :list-name)
    (map (fn [[list-name cards]] {:list-name list-name :cards (sort-cards cards)}))
    (sort-by :list-name)
    vec))

(defn date-str-add-days [date-str plus-days]
  (->> date-str
    (timeformat/parse date-formatter)
    (#(time/plus % (time/days plus-days)))
    (timeformat/unparse date-formatter)))

(defn get-cards 
  ([result-channel error-channel board date-from date-to is-retry]
    (.get js/Trello
      (str "boards/" board "/actions?filter=updateCard:closed,commentCard"
        "&since=" date-from
        "&before=" date-to)
      (fn [e] (async/put! result-channel (create-lists (js->clj e :keywordize-keys true))))
      (fn [e]
        (do
          (async/put! error-channel (str "failed to get cards: " (.. e -responseText)))
          (authorize-trello)
          (if (not is-retry)
            (get-cards result-channel error-channel board date-from date-to true))))))
  ([result-channel error-channel board date-from date-to]
    (get-cards result-channel error-channel board date-from date-to false))
  ([result-channel error-channel board date]
    (get-cards result-channel error-channel board date (date-str-add-days date 1) false)))

(defn handle-change [e edit-key owner]
  (om/set-state! owner edit-key (.. e -target -value)))

(defn fetch-cards-view [data owner]
  (reify
    om/IInitState
    (init-state [_]
      {:date (today) :board "559e4136beec0eaeba2cf49f"})
    om/IWillMount
    (will-mount [_]
      (get-cards (om/get-state owner :cards-channel) (om/get-state owner :error-channel) (om/get-state owner :board) (om/get-state owner :date)))
    om/IRenderState
    (render-state [this {:keys [cards-channel error-channel] :as state}]
      (dom/div #js {:id "fetcher"}
        (let [fetch (fn [_] (get-cards cards-channel error-channel (om/get-state owner :board) (om/get-state owner :date)))]
          (dom/div nil
            (dom/input #js {
              :type "text"
              :value (:board state)
              :onChange #(handle-change % :board owner)
              :onBlur fetch
              :onKeyPress (fn [e] (if (= "Enter" (.. e -key)) (fetch e)))
              })
            (dom/button #js {
              :onClick (fn [_] (do
                (om/update-state! owner :date #(date-str-add-days % -1))
                (fetch _)))}
              "<")
            (dom/input #js {
              :type "text"
              :value (:date state)
              :onChange #(handle-change % :date owner)
              :onBlur fetch
              :onKeyPress (fn [e] (if (= "Enter" (.. e -key)) (fetch e)))
              })
            (dom/button #js {
              :onClick (fn [_] (do
                (om/update-state! owner :date #(date-str-add-days % 1))
                (fetch _)))}
              ">")))))))

(defn card-view [card owner]
  (reify
    om/IInitState
    (init-state [_]
      {:show-actions false})
    om/IRenderState
    (render-state [this {:keys [show-actions]}]
      (dom/div #js {:className "trello-card"}
        (render-card owner card show-actions)))))

(defn trello-list-view [data owner]
  (reify
    om/IRender
    (render [_]
      (dom/div #js {:className "trello-list"}
        (dom/h2 nil (:list-name data))
        (apply dom/div nil
          (om/build-all card-view (:cards data)))))))

(defn cards-dash-view [data owner]
  (reify
    om/IInitState
    (init-state [_]
      {:cards-channel (async/chan) :error-channel (async/chan)})
    om/IWillMount
    (will-mount [_]
      (let [cards-channel (om/get-state owner :cards-channel) error-channel (om/get-state owner :error-channel)]
        (go (loop []
          (let [[incoming-data channel] (alts! [cards-channel error-channel])]
            (if (= channel cards-channel)
              (do
                (om/set-state! owner :error nil)
                (om/update! data :lists incoming-data))
              (om/set-state! owner :error incoming-data))
            (recur))))))
    om/IRenderState
    (render-state [this {:keys [error] :as state}]
      (dom/div nil
        (dom/h1 nil "Trello board activity")
        (om/build fetch-cards-view (:date-range data) {:init-state state})
        (dom/span (display (not (nil? error)) {:className "error"}) error)
        (apply dom/div #js {:className "lists-board"}
          (om/build-all trello-list-view (:lists data)))))))

(om/root
  cards-dash-view
  app-state
  {:target (.getElementById js/document "cards-dash")})

(defn comment-view [data owner]
  (reify
    om/IInitState
    (init-state [_]
      {:comment nil})
    om/IWillMount
    (will-mount [_]
      (do
        (.addEventListener js/window "keydown" (fn [e] (if (= "Escape" (.. e -code)) (om/set-state! owner :comment nil))))
        (go
          (loop []
            (let [c (<! comments-channel)]
              (om/set-state! owner :comment c)
              (recur))))))
    om/IRenderState
    (render-state [this {:keys [comment] :as state}]
      (dom/div (display (not (nil? comment)) {
        :className "comment-view"})
        (dom/span nil comment)
        (dom/br nil)
        (dom/button #js {:onClick (fn [_] (om/set-state! owner :comment nil))} "close")))))

(om/root
  comment-view
  app-state
  {:target (.getElementById js/document "comment")})

(defn on-js-reload []
  ;; optionally touch your app-state to force rerendering depending on
  ;; your application
  ;; (swap! app-state update-in [:__figwheel_counter] inc)
)
